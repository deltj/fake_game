#ifndef VECTOR_HPP
#define VECTOR_HPP

#include <ostream>

#include <fake_game/serialization.hpp>

namespace fake_game
{

class Vector3d : public Serializable
{
public:
    Vector3d();
    Vector3d(double x, double y, double z);
    Vector3d(const Vector3d &other);
    virtual ~Vector3d();

    virtual size_t writeSize() const;

    virtual size_t read(const uint8_t * const buf, const size_t buf_len);

    virtual size_t write(uint8_t * const buf, const size_t buf_len) const;    

    Vector3d & operator=(const Vector3d &rhs);

    Vector3d & operator+=(const Vector3d &rhs);

    Vector3d & operator-=(const Vector3d &rhs);

    Vector3d & operator*=(const double &rhs);

    Vector3d & operator/=(const double &rhs);

    const Vector3d operator+(const Vector3d &rhs) const;

    const Vector3d operator-(const Vector3d &rhs) const;

    const Vector3d operator*(const double &rhs) const;

    const Vector3d operator/(const double &rhs) const;

    bool operator==(const Vector3d &other) const;

    bool operator!=(const Vector3d &other) const;

    double magnitude() const;

    Vector3d normalize() const;

    double distance(const Vector3d &other);

    Vector3d cross(const Vector3d &v) const;

    double dot(const Vector3d &v) const;


    double x;
    double y;
    double z;
};

std::ostream& operator<<(std::ostream& os, const Vector3d& v);

}

#endif

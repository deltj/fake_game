#ifndef SERIALIZATION_HPP
#define SERIALIZATION_HPP

#include <cstddef>
#include <cstdint>

namespace fake_game
{

size_t read(const uint8_t * const buf, const size_t &buf_len, uint8_t &value);
size_t read(const uint8_t * const buf, const size_t &buf_len, uint16_t &value);
size_t read(const uint8_t * const buf, const size_t &buf_len, uint32_t &value);
size_t read(const uint8_t * const buf, const size_t &buf_len, double &value);

size_t write(uint8_t * const buf, const size_t &buf_len, const uint8_t &value);
size_t write(uint8_t * const buf, const size_t &buf_len, const uint16_t &value);
size_t write(uint8_t * const buf, const size_t &buf_len, const uint32_t &value);
size_t write(uint8_t * const buf, const size_t &buf_len, const double &value);

class Serializable
{
public:
    virtual size_t writeSize() const = 0;
    virtual size_t read(const uint8_t * const buf, const size_t buf_len) = 0;
    virtual size_t write(uint8_t * const buf, const size_t buf_len) const = 0;
};


}
#endif

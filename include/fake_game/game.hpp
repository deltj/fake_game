#ifndef GAME_HPP
#define GAME_HPP

#include "fake_game/game_defs.hpp"
#include "fake_game/vector.hpp"
#include "fake_game/serialization.hpp"

#define FG_MAGIC 0x41595959
#define FG_HEADER_LEN 8

namespace fake_game
{

//  Boundary plane normals

static const Vector3d nxmax = Vector3d(-1.0, 0.0, 0.0);
static const Vector3d nxmin = Vector3d(1.0, 0.0, 0.0);
static const Vector3d nymax = Vector3d(0.0, -1.0, 0.0);
static const Vector3d nymin = Vector3d(0.0, 1.0, 0.0);
static const Vector3d nzmax = Vector3d(0.0, 0.0, -1.0);
static const Vector3d nzmin = Vector3d(0.0, 0.0, 1.0);

enum MESSAGE_TYPE
{
    PING = 0,
    PONG,
    ENTITY_STATE_UPDATE,

    MESSAGE_TYPE_MAX
};

class FGHeader : public Serializable
{
public:
    FGHeader();
    FGHeader(const uint32_t &magic, const uint8_t &seq, const uint8_t &type, const uint8_t &len, const uint8_t &flags);
    FGHeader(const FGHeader &other);
    virtual ~FGHeader();

    FGHeader & operator=(const FGHeader &rhs);
    bool operator==(const FGHeader &other) const;
    bool operator!=(const FGHeader &other) const;

    virtual size_t writeSize() const;
    virtual size_t read(const uint8_t * const buf, const size_t buf_len);
    virtual size_t write(uint8_t * const buf, const size_t buf_len) const;

    uint32_t magic;
    uint8_t seq;
    uint8_t type;
    uint8_t len;
    uint8_t flags;
};

std::ostream& operator<<(std::ostream& os, const FGHeader& h);

class EntityState : public Serializable
{
public:
    EntityState();
    EntityState(const uint32_t &id, const Vector3d &pos, const Vector3d &vel);
    EntityState(const EntityState &other);
    virtual ~EntityState();

    EntityState & operator=(const EntityState &rhs);
    bool operator==(const EntityState &other) const;
    bool operator!=(const EntityState &other) const;

    virtual size_t writeSize() const;
    virtual size_t read(const uint8_t * const buf, const size_t buf_len);
    virtual size_t write(uint8_t * const buf, const size_t buf_len) const;

    void update();

    uint32_t id;
    Vector3d pos;
    Vector3d vel;
};

std::ostream& operator<<(std::ostream& os, const EntityState& s);

}

#endif

#include "fake_game/game.hpp"

#define BOOST_TEST_MODULE GameTest
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace fake_game;

BOOST_AUTO_TEST_CASE( FGHeader_rw )
{
    const size_t BUFFER_LEN = 1024;
    uint8_t buf[BUFFER_LEN];

    FGHeader h1;
    h1.magic = 0xBAADF00D;
    h1.seq = 1;
    h1.type = 128;
    h1.flags = 0xFF;

    const size_t num_written = h1.write(buf, BUFFER_LEN);
    BOOST_CHECK_EQUAL(8, num_written);

    FGHeader h2;
    const size_t num_read = h2.read(buf, BUFFER_LEN);
    BOOST_CHECK_EQUAL(8, num_read);
    BOOST_CHECK_EQUAL(h1, h2);

    BOOST_CHECK_EQUAL(0xBAADF00D, h2.magic);
    BOOST_CHECK_EQUAL(1, h2.seq);
    BOOST_CHECK_EQUAL(128, h2.type);
    BOOST_CHECK_EQUAL(0xFF, h2.flags);
}

BOOST_AUTO_TEST_CASE( EntityState_rw )
{
    const size_t BUFFER_LEN = 1024;
    uint8_t buf[BUFFER_LEN];

    EntityState s1;
    s1.id = 1;
    s1.pos = Vector3d(0.1, 0.2, 0.3);
    s1.vel = Vector3d(0.4, 0.5, 0.6);

    const size_t num_written = s1.write(buf, BUFFER_LEN);
    BOOST_CHECK_EQUAL(52, num_written);

    EntityState s2;
    const size_t num_read = s2.read(buf, BUFFER_LEN);
    BOOST_CHECK_EQUAL(52, num_read);
    BOOST_CHECK_EQUAL(s1, s2);

    BOOST_CHECK_EQUAL(1, s2.id);
    BOOST_CHECK_EQUAL(Vector3d(0.1, 0.2, 0.3), s2.pos);
    BOOST_CHECK_EQUAL(Vector3d(0.4, 0.5, 0.6), s2.vel);
}

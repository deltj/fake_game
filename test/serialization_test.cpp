#include "fake_game/serialization.hpp"

#define BOOST_TEST_MODULE SerializationTest
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace fake_game;

const size_t BUFFER_LEN = 1024;
uint8_t buf[BUFFER_LEN];

BOOST_AUTO_TEST_CASE( rw_uint8 )
{
    const uint8_t value1 = 0x55;
    const size_t num_written = write(buf, BUFFER_LEN, value1);
    BOOST_CHECK_EQUAL(1, num_written);
    BOOST_CHECK_EQUAL(0x55, buf[0]);

    uint8_t value2;
    const size_t num_read = read(buf, BUFFER_LEN, value2);
    BOOST_CHECK_EQUAL(1, num_read);
    BOOST_CHECK_EQUAL(value1, value2);
}

BOOST_AUTO_TEST_CASE( rw_uint16 )
{
    uint16_t value1 = 0x55AA;
    const size_t num_written = write(buf, BUFFER_LEN, value1);
    BOOST_CHECK_EQUAL(2, num_written);
    BOOST_CHECK_EQUAL(0x55, buf[0]);
    BOOST_CHECK_EQUAL(0xAA, buf[1]);

    uint16_t value2;
    const size_t num_read = read(buf, BUFFER_LEN, value2);
    BOOST_CHECK_EQUAL(2, num_read);
    BOOST_CHECK_EQUAL(value1, value2);
}

BOOST_AUTO_TEST_CASE( rw_uint32 )
{
    uint32_t value1 = 0xBAADF00D;
    const size_t num_written = write(buf, BUFFER_LEN, value1);
    BOOST_CHECK_EQUAL(4, num_written);
    BOOST_CHECK_EQUAL(0xBA, buf[0]);
    BOOST_CHECK_EQUAL(0xAD, buf[1]);
    BOOST_CHECK_EQUAL(0xF0, buf[2]);
    BOOST_CHECK_EQUAL(0x0D, buf[3]);

    uint32_t value2;
    const size_t num_read = read(buf, BUFFER_LEN, value2);
    BOOST_CHECK_EQUAL(4, num_read);
    BOOST_CHECK_EQUAL(value1, value2);
}

BOOST_AUTO_TEST_CASE( rw_double )
{
    double value1 = 2.718281828459045;
    const size_t num_written = write(buf, BUFFER_LEN, value1);
    BOOST_CHECK_EQUAL(8, num_written);

    double value2;
    const size_t num_read = read(buf, BUFFER_LEN, value2);
    BOOST_CHECK_EQUAL(8, num_read);
    BOOST_CHECK_EQUAL(value1, value2);
}

#include "fake_game/vector.hpp"

#define BOOST_TEST_MODULE VectorTest
#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace fake_game;

BOOST_AUTO_TEST_CASE( Vector3d_ctor )
{
    Vector3d v;

    BOOST_CHECK_EQUAL(0.0, v.x);
    BOOST_CHECK_EQUAL(0.0, v.y);
    BOOST_CHECK_EQUAL(0.0, v.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_ctor2 )
{
    Vector3d v(1.0, 2.0, 3.0);

    BOOST_CHECK_EQUAL(1.0, v.x);
    BOOST_CHECK_EQUAL(2.0, v.y);
    BOOST_CHECK_EQUAL(3.0, v.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_copy_ctor )
{
    Vector3d v1(1.0, 2.0, 3.0);
    Vector3d v2 = v1;

    BOOST_CHECK_EQUAL(1.0, v2.x);
    BOOST_CHECK_EQUAL(2.0, v2.y);
    BOOST_CHECK_EQUAL(3.0, v2.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_rw )
{
    const size_t BUFFER_LEN = 1024;
    uint8_t buf[BUFFER_LEN];

    const Vector3d v1(1.0, 2.0, 3.0);
    const size_t num_written = v1.write(buf, BUFFER_LEN);
    BOOST_CHECK_EQUAL(24, num_written);

    Vector3d v2;
    const size_t num_read = v2.read(buf, BUFFER_LEN);
    BOOST_CHECK_EQUAL(24, num_read);
    BOOST_CHECK_EQUAL(v1, v2);

    BOOST_CHECK_EQUAL(1.0, v2.x);
    BOOST_CHECK_EQUAL(2.0, v2.y);
    BOOST_CHECK_EQUAL(3.0, v2.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_assign )
{
    Vector3d v1(1.0, 2.0, 3.0);
    Vector3d v2;
    v2 = v1;

    BOOST_CHECK_EQUAL(1.0, v2.x);
    BOOST_CHECK_EQUAL(2.0, v2.y);
    BOOST_CHECK_EQUAL(3.0, v2.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_compound_add )
{
    Vector3d v1(1.0, 2.0, 3.0);
    Vector3d v2(1.0, 2.0, 3.0);
    v2 += v1;

    BOOST_CHECK_EQUAL(2.0, v2.x);
    BOOST_CHECK_EQUAL(4.0, v2.y);
    BOOST_CHECK_EQUAL(6.0, v2.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_compound_sub )
{
    Vector3d v1(1.0, 2.0, 3.0);
    Vector3d v2(1.0, 2.0, 3.0);
    v2 -= v1;

    BOOST_CHECK_EQUAL(0.0, v2.x);
    BOOST_CHECK_EQUAL(0.0, v2.y);
    BOOST_CHECK_EQUAL(0.0, v2.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_compound_mul_scalar )
{
    Vector3d v1(1.0, 2.0, 3.0);
    v1 *= 2.0;

    BOOST_CHECK_EQUAL(2.0, v1.x);
    BOOST_CHECK_EQUAL(4.0, v1.y);
    BOOST_CHECK_EQUAL(6.0, v1.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_compound_div_scalar )
{
    Vector3d v1(1.0, 2.0, 3.0);
    v1 /= 2.0;

    BOOST_CHECK_EQUAL(0.5, v1.x);
    BOOST_CHECK_EQUAL(1.0, v1.y);
    BOOST_CHECK_EQUAL(1.5, v1.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_add )
{
    Vector3d v1(1.0, 1.0, 1.0);
    Vector3d v2(1.0, 1.0, 1.0);
    Vector3d v3 = v1 + v2;

    BOOST_CHECK_EQUAL(2.0, v3.x);
    BOOST_CHECK_EQUAL(2.0, v3.y);
    BOOST_CHECK_EQUAL(2.0, v3.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_sub )
{
    Vector3d v1(1.0, 1.0, 1.0);
    Vector3d v2(1.0, 1.0, 1.0);
    Vector3d v3 = v1 - v2;

    BOOST_CHECK_EQUAL(0.0, v3.x);
    BOOST_CHECK_EQUAL(0.0, v3.y);
    BOOST_CHECK_EQUAL(0.0, v3.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_mul_scalar )
{
    Vector3d v1(1.0, 1.0, 1.0);
    Vector3d v2 = v1 * 2.0;

    BOOST_CHECK_EQUAL(2.0, v2.x);
    BOOST_CHECK_EQUAL(2.0, v2.y);
    BOOST_CHECK_EQUAL(2.0, v2.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_div_scalar )
{
    Vector3d v1(1.0, 1.0, 1.0);
    Vector3d v2 = v1 / 2.0;

    BOOST_CHECK_EQUAL(0.5, v2.x);
    BOOST_CHECK_EQUAL(0.5, v2.y);
    BOOST_CHECK_EQUAL(0.5, v2.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_equality )
{
    Vector3d v1(1.0, 1.0, 1.0);
    Vector3d v2(1.0, 1.0, 1.0);

    BOOST_CHECK_EQUAL(v1, v2);
}

BOOST_AUTO_TEST_CASE( Vector3d_inequality )
{
    Vector3d v1(1.0, 1.0, 1.0);
    Vector3d v2(1.0, 1.0, 1.1);

    BOOST_CHECK(v1 != v2);
}

BOOST_AUTO_TEST_CASE( Vector3d_magnitude )
{
    Vector3d v(1.0, 0.0, 0.0);

    BOOST_CHECK_EQUAL(1.0, v.magnitude());
}

BOOST_AUTO_TEST_CASE( Vector3d_normalize )
{
    Vector3d v1(100.0, 100.0, 100.0);
    Vector3d v2 = v1.normalize();

    BOOST_CHECK_EQUAL(1.0, v2.magnitude());
}

BOOST_AUTO_TEST_CASE( Vector3d_distance )
{
    Vector3d v1(  0.0, 0.0, 0.0);
    Vector3d v2(100.0, 0.0, 0.0);

    BOOST_CHECK_EQUAL(100.0, v1.distance(v2));
}

BOOST_AUTO_TEST_CASE( Vector3d_cross )
{
    const Vector3d u( 5.0, 1.0, 2.0);
    const Vector3d v(-2.0, 0.0, 1.0);
    const Vector3d w = u.cross(v);

    BOOST_CHECK_EQUAL(1.0, w.x);
    BOOST_CHECK_EQUAL(-9.0, w.y);
    BOOST_CHECK_EQUAL(2.0, w.z);
}

BOOST_AUTO_TEST_CASE( Vector3d_dot )
{
    const Vector3d u( 3.0, 5.0, 2.0);
    const Vector3d v(-1.0, 3.0, 0.0);
    const double p = u.dot(v);

    BOOST_CHECK_EQUAL(12.0, p);
}

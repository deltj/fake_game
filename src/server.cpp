/*
 * Fake Game Server
 */
#include <cmath>
#include <cstdlib>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>

extern "C" {
    #include <signal.h>
}

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/enable_shared_from_this.hpp>

#include "fake_game/vector.hpp"
#include "fake_game/game.hpp"

using namespace fake_game;

using namespace std::chrono_literals;
using boost::asio::ip::tcp;

typedef std::chrono::high_resolution_clock hrc;

bool quit = false;

const double DEGREES_TO_RADIANS = 0.01745329;

//  Server frame rate in Hz
const double SERVER_FRAME_RATE = 10.0;

//  Server frame interval
const std::chrono::milliseconds SERVER_FRAME_INTERVAL = std::chrono::milliseconds(llround((1.0 / SERVER_FRAME_RATE) * 1000.0));

const int NUM_NPCS = 1000;
const double NPC_VELOCITY = 4.0;
const double NPC_DETECT_RADIUS = 100.0;

class NetworkClient;

std::vector<std::shared_ptr<EntityState>> entities;
std::vector<std::shared_ptr<NetworkClient>> clients;

/**
 * This class represents a single connection to our TCP server
*/
class TCPConnection : public boost::enable_shared_from_this<TCPConnection>
{
public:
    typedef boost::shared_ptr<TCPConnection> pointer;

    static pointer create(boost::asio::io_context& io_context)
    {
        return pointer(new TCPConnection(io_context));
    }

    tcp::socket& socket()
    {
        return socket_;
    }

    void start()
    {
    }

    void write(const std::string &message)
    {
        boost::asio::async_write(socket_, boost::asio::buffer(message_),
            boost::bind(&TCPConnection::handleWrite, shared_from_this(),
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
    }

    void write(const uint8_t *buf, const size_t &size)
    {
        boost::asio::async_write(socket_, boost::asio::buffer(buf, size),
            boost::bind(&TCPConnection::handleWrite, shared_from_this(),
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
    }

private:
    TCPConnection(boost::asio::io_context& io_context) : socket_(io_context)
    {
    }

    void handleWrite(const boost::system::error_code& /*error*/,
        size_t /*bytes_transferred*/)
    {
    }

    tcp::socket socket_;
    std::string message_;
};

/**
 * This class wraps the network connection(s) for a client for easier client updating/management
*/
class NetworkClient
{
public:
    NetworkClient(const TCPConnection::pointer conn) : tcp_conn(conn),
        server_seq(0)
    {

    }

    void sendEntityStateUpdate(std::shared_ptr<EntityState> entity)
    {
        auto h = FGHeader(FG_MAGIC, server_seq, ENTITY_STATE_UPDATE, entity->writeSize(), 0);

        const size_t hsz = h.write(txBuffer, txBufferSize);
        const size_t ssz = entity->write(txBuffer + hsz, txBufferSize - hsz);

        tcp_conn->write(txBuffer, hsz + ssz);

        server_seq++;
    }

private:
    NetworkClient();

    boost::shared_ptr<TCPConnection> tcp_conn;
    uint16_t server_seq;
    static const size_t txBufferSize = 1024;
    uint8_t txBuffer[txBufferSize];
};

/**
 * This class implements a listening TCP/IP server
*/
class TCPServer
{
public:
    TCPServer(boost::asio::io_context &io_context) : io_context_(io_context),
        acceptor_(io_context, tcp::endpoint(tcp::v4(), 6208)),
        signals_(io_context)
    {
        signals_.add(SIGINT);
        signals_.add(SIGTERM);
        signals_.async_wait(boost::bind(&TCPServer::handleStop, this));
        startAccept();
    }

private:
    TCPServer();

    void startAccept()
    {
        TCPConnection::pointer new_connection = TCPConnection::create(io_context_);

        acceptor_.async_accept(new_connection->socket(),
            boost::bind(&TCPServer::handleAccept, this, new_connection,
            boost::asio::placeholders::error));
    }

    void handleAccept(TCPConnection::pointer new_connection,
        const boost::system::error_code& error)
    {
        if(!error)
        {
            new_connection->start();

            std::shared_ptr<NetworkClient> client = std::make_shared<NetworkClient>(new_connection);
            clients.push_back(client);
        }

        startAccept();
    }

    void handleStop()
    {
        std::cout << "boost signal handler" << std::endl;
        quit = true;
        acceptor_.close();
        io_context_.stop();
    }

    boost::asio::io_context& io_context_;
    tcp::acceptor acceptor_;
    boost::asio::signal_set signals_;
};

void tcpServerThreadFn()
{
    boost::asio::io_context io_context;
    TCPServer server(io_context);

    io_context.run();
}

void udpServerThreadFn()
{
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

int main(int argc, char *argv[])
{
    //  Spawn networking threads
    std::thread tcpServerThread(tcpServerThreadFn);
    std::thread udpServerThread(udpServerThreadFn);

    //  Generate NPCs with random position and velocity
    for(int i=0; i<NUM_NPCS; i++)
    {
        const double x = rand() % FG_WORLD_SIZE;
        const double y = rand() % FG_WORLD_SIZE;
        const double z = rand() % FG_WORLD_SIZE;

        std::shared_ptr<EntityState> e = std::make_shared<EntityState>();
        e->id = i;
        e->pos.x = x;
        e->pos.y = y;
        e->pos.z = z;

        const double theta = rand() % 360 * DEGREES_TO_RADIANS;
        e->vel.x = NPC_VELOCITY * cos(theta);
        e->vel.y = NPC_VELOCITY * sin(theta);
        e->vel.z = 0.0;

        entities.push_back(e);
    }

    //  Game loop - update game state at a fixed rate
    uint64_t frame_num = 0;
    while(!quit)
    {
        auto t0 = hrc::now();

        //  Update all entities
        for(auto e : entities)
        {
            e->update();
        }

        //  Provide entity state updates to all clients
        for(auto c : clients)
        {
            //  Sent client c and update for entity e
            for(auto e : entities)
            {
                c->sendEntityStateUpdate(e);
            }
        }

        auto t1 = hrc::now();
        std::chrono::milliseconds d = std::chrono::duration_cast<std::chrono::milliseconds>(t1 - t0);
        std::chrono::milliseconds s = SERVER_FRAME_INTERVAL - d;

        std::cout << "game loop took " << d.count() << " ms, sleeping for " << s.count() << " ms" << std::endl;

        frame_num++;
        std::this_thread::sleep_for(s);
    }

    tcpServerThread.join();
    udpServerThread.join();

    std::cout << "done" << std::endl;

    return EXIT_SUCCESS;
}
#include "fake_game/vector.hpp"

#include <cmath>
#include <sstream>

namespace fake_game
{

Vector3d::Vector3d() : x(0.0),
    y(0.0),
    z(0.0)
{
}

Vector3d::Vector3d(double x, double y, double z) : x(x),
    y(y),
    z(z)
{
}

Vector3d::Vector3d(const Vector3d &other) : x(other.x),
    y(other.y),
    z(other.z)
{
}

Vector3d::~Vector3d()
{
}

size_t Vector3d::writeSize() const
{
    return 24;
}

size_t Vector3d::read(const uint8_t * const buf, const size_t buf_len)
{
    size_t read_size = 0;

    read_size += fake_game::read(buf, buf_len, x);
    read_size += fake_game::read(buf + read_size, buf_len - read_size, y);
    read_size += fake_game::read(buf + read_size, buf_len - read_size, z);

    return read_size;
}

size_t Vector3d::write(uint8_t * const buf, const size_t buf_len) const
{
    size_t write_size = 0;

    write_size += fake_game::write(buf, buf_len, x);
    write_size += fake_game::write(buf + write_size, buf_len - write_size, y);
    write_size += fake_game::write(buf + write_size, buf_len - write_size, z);

    return write_size;
}

Vector3d & Vector3d::operator=(const Vector3d &rhs)
{
    if (this != &rhs) {
        x = rhs.x;
        y = rhs.y;
        z = rhs.z;
    }
    return *this;
}

Vector3d & Vector3d::operator+=(const Vector3d &rhs)
{
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return *this;
}

Vector3d & Vector3d::operator-=(const Vector3d &rhs)
{
    x -= rhs.x;
    y -= rhs.y;
    z -= rhs.z;
    return *this;
}

Vector3d & Vector3d::operator*=(const double &rhs)
{
    x *= rhs;
    y *= rhs;
    z *= rhs;
    return *this;
}

Vector3d & Vector3d::operator/=(const double &rhs)
{
    x /= rhs;
    y /= rhs;
    z /= rhs;
    return *this;
}

const Vector3d Vector3d::operator+(const Vector3d &rhs) const
{
    Vector3d result = *this;
    result += rhs;
    return result;
}

const Vector3d Vector3d::operator-(const Vector3d &rhs) const
{
    Vector3d result = *this;
    result -= rhs;
    return result;
}

const Vector3d Vector3d::operator*(const double &rhs) const
{
    Vector3d result = *this;
    result *= rhs;
    return result;
}

const Vector3d Vector3d::operator/(const double &rhs) const
{
    Vector3d result = *this;
    result /= rhs;
    return result;
}

bool Vector3d::operator==(const Vector3d &other) const
{
    return x == other.x && 
           y == other.y &&
           z == other.z;
}

bool Vector3d::operator!=(const Vector3d &other) const
{
    return !(*this == other);
}

double Vector3d::magnitude() const
{
    return sqrt(x * x + y * y + z * z);
}

Vector3d Vector3d::normalize() const
{
    const double m = magnitude();
    return *this / m;
}

double Vector3d::distance(const Vector3d &other)
{
    const double dx = this->x - other.x;
    const double dy = this->y - other.y;
    const double dz = this->z - other.z;
    const double c = dx * dx + dy * dy + dz * dz;
    return sqrt(c);
}

Vector3d Vector3d::cross(const Vector3d &v) const
{
    return Vector3d(y * v.z - z * v.y, -(x * v.z - z * v.x), x * v.y - y * v.x);
}

double Vector3d::dot(const Vector3d &v) const
{
    return x * v.x + y * v.y + z * v.z;
}

std::ostream& operator<<(std::ostream& os, const Vector3d& v)
{
    os << "vector3d {" << v.x << "," << v.y << "," << v.z << "}";
    return os;
}

}
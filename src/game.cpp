#include "fake_game/game.hpp"
#include "fake_game/game_defs.hpp"

#include <iomanip>
#include <iostream>

namespace fake_game
{

FGHeader::FGHeader() : magic(FG_MAGIC),
    seq(0),
    type(0),
    flags(0)
{
}

FGHeader::FGHeader(const uint32_t &magic, const uint8_t &seq, const uint8_t &type, const uint8_t &len, const uint8_t &flags) : magic(magic),
    seq(seq),
    type(type),
    len(len),
    flags(flags)
{
}  

FGHeader::FGHeader(const FGHeader &other) : magic(other.magic),
    seq(other.seq),
    type(other.type),
    len(other.len),
    flags(other.flags)
{
}

FGHeader::~FGHeader()
{
}

FGHeader & FGHeader::operator=(const FGHeader &rhs)
{
    if (this != &rhs) {
        magic = rhs.magic;
        seq = rhs.seq;
        type = rhs.type;
        len = rhs.len;
        flags = rhs.flags;
    }
    return *this;
}

bool FGHeader::operator==(const FGHeader &other) const
{
    return magic == other.magic && 
           seq == other.seq &&
           type == other.type &&
           len == other.len &&
           flags == other.flags;
}

bool FGHeader::operator!=(const FGHeader &other) const
{
    return !(*this == other);
}

size_t FGHeader::writeSize() const
{
    return 8;
}

size_t FGHeader::read(const uint8_t * const buf, const size_t buf_len)
{
    size_t read_size = 0;

    read_size += fake_game::read(buf, buf_len, magic);
    read_size += fake_game::read(buf + read_size, buf_len - read_size, seq);
    read_size += fake_game::read(buf + read_size, buf_len - read_size, type);
    read_size += fake_game::read(buf + read_size, buf_len - read_size, len);
    read_size += fake_game::read(buf + read_size, buf_len - read_size, flags);

    return read_size;
}

size_t FGHeader::write(uint8_t * const buf, const size_t buf_len) const
{
    size_t write_size = 0;

    write_size += fake_game::write(buf, buf_len, magic);
    write_size += fake_game::write(buf + write_size, buf_len - write_size, seq);
    write_size += fake_game::write(buf + write_size, buf_len - write_size, type);
    write_size += fake_game::write(buf + write_size, buf_len - write_size, len);
    write_size += fake_game::write(buf + write_size, buf_len - write_size, flags);

    return write_size;
}

std::ostream& operator<<(std::ostream& os, const FGHeader& h)
{
    os << "FGHeader {";
    os << "magic=" << std::hex << std::setw(8) << std::setfill('0') << h.magic << ",";
    os << "seq=" << std::dec << std::setw(0) << (unsigned int)h.seq << ",";
    os << "type=" << (unsigned int)h.type << ",";
    os << "len=" << (unsigned int)h.len << ",";
    os << "flags=" <<std::hex << std::setw(2) << std::setfill('0') << (unsigned int)h.flags << "}";
    os << std::dec << std::setw(0);
    return os;
}

EntityState::EntityState()
{
}

EntityState::EntityState(const uint32_t &id, const Vector3d &pos, const Vector3d &vel) : id(id),
    pos(pos),
    vel(vel)
{
}

EntityState::EntityState(const EntityState &other) : id(other.id),
    pos(other.pos),
    vel(other.vel)
{
}

EntityState::~EntityState()
{
}

EntityState & EntityState::operator=(const EntityState &rhs)
{
    if (this != &rhs) {
        id = rhs.id;
        pos = rhs.pos;
        vel = rhs.vel;
    }
    return *this;
}

bool EntityState::operator==(const EntityState &other) const
{
    return id == other.id && 
           pos == other.pos &&
           vel == other.vel;
}

bool EntityState::operator!=(const EntityState &other) const
{
    return !(*this == other);
}

size_t EntityState::writeSize() const
{
    return 52;
}

size_t EntityState::read(const uint8_t * const buf, const size_t buf_len)
{
    size_t read_size = 0;

    read_size += fake_game::read(buf, buf_len, id);
    read_size += pos.read(buf + read_size, buf_len - read_size);
    read_size += vel.read(buf + read_size, buf_len - read_size);

    return read_size;
}

size_t EntityState::write(uint8_t * const buf, const size_t buf_len) const
{
    size_t write_size = 0;

    write_size += fake_game::write(buf, buf_len, id);
    write_size += pos.write(buf + write_size, buf_len - write_size);
    write_size += vel.write(buf + write_size, buf_len - write_size);

    return write_size;
}

void EntityState::update()
{
    //std::cout << "pos: " << pos << std::endl;

    pos += vel;

    bool collision = false;
    Vector3d u;

    //std::cout << "old vel: " << vel << std::endl;

    if(pos.x > FG_WORLD_SIZE)
    {
        //  Collision with upper x axis boundary plane
        u = nxmax * vel.dot(nxmax);
        collision = true;
    }
    else if(pos.x < 0.0)
    {
        //  Collision with lower x axis boundary plane
        u = nxmin * vel.dot(nxmin);
        collision = true;
    }

    if(pos.y > FG_WORLD_SIZE)
    {
        //  Collision with upper y axis boundary plane
        u = nymax * vel.dot(nymax);
        collision = true;
    }
    else if(pos.y < 0.0)
    {
        //  Collision with lower y axis boundary plane
        u = nymin * vel.dot(nymin);
        collision = true;
    }

    if(pos.z > FG_WORLD_SIZE)
    {
        //  Collision with upper z axis boundary plane
        u = nzmax * vel.dot(nzmax);
        collision = true;
    }
    else if(pos.z < 0.0)
    {
        //  Collision with lower z axis boundary plane
        u = nzmin * vel.dot(nzmin);
        collision = true;
    }

    if(collision)
    {
        const Vector3d w = vel - u;
        vel = w - u;

        //  Handle the bounce now rather than waiting until next frame, so the
        //  entity doesn't appear to pass through the boundary plane
        pos += vel;
    }

    //std::cout << "new vel: " << vel << std::endl;
}

std::ostream& operator<<(std::ostream& os, const EntityState& s)
{
    os << "EntityState {" << s.id << "," << s.pos << "," << s.vel << "}";
    return os;
}

}

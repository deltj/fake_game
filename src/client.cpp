/*
 * Fake Game Client
 */
#include <cstdlib>
#include <iomanip>
#include <fstream>
#include <iostream>
#include <unordered_map>

#include <boost/asio.hpp>
#include <boost/bind/bind.hpp>
#include <boost/enable_shared_from_this.hpp>

#include <SDL2/SDL.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "fake_game/game_defs.hpp"
#include "fake_game/game.hpp"

using boost::asio::ip::tcp;
using namespace fake_game;

bool quit = false;
std::unordered_map<uint32_t, std::shared_ptr<EntityState>> entity_map;

/**
 * Create a set of vertices to render a cube centered on the specified point
 * @param c The point at the center of the cube
 * @param v The destination array to store the vertices
 * @param n The length (number of floats) of the destination array
*/
void makeCubeVertices(const glm::vec3 &c, float *v, const size_t n)
{
    static const float d = (float)FG_ENTITY_CUBE_SIZE / 2.0;

    if(n < 108) return;

    //  Each triplet of floats is a vertex in one of the twelve triangles used to render the cube
    v[  0] = c.x-d; v[  1] = c.y-d; v[  2] = c.z-d; // triangle 1 : begin
    v[  3] = c.x-d; v[  4] = c.y-d; v[  5] = c.z+d;
    v[  6] = c.x-d; v[  7] = c.y+d; v[  8] = c.z+d; // triangle 1 : end
    v[  9] = c.x+d; v[ 10] = c.y+d; v[ 11] = c.z-d; // triangle 2 : begin
    v[ 12] = c.x-d; v[ 13] = c.y-d; v[ 14] = c.z-d;
    v[ 15] = c.x-d; v[ 16] = c.y+d; v[ 17] = c.z-d; // triangle 2 : end
    v[ 18] = c.x+d; v[ 19] = c.y-d; v[ 20] = c.z+d;
    v[ 21] = c.x-d; v[ 22] = c.y-d; v[ 23] = c.z-d;
    v[ 24] = c.x+d; v[ 25] = c.y-d; v[ 26] = c.z-d;
    v[ 27] = c.x+d; v[ 28] = c.y+d; v[ 29] = c.z-d;
    v[ 30] = c.x+d; v[ 31] = c.y-d; v[ 32] = c.z-d;
    v[ 33] = c.x-d; v[ 34] = c.y-d; v[ 35] = c.z-d;
    v[ 36] = c.x-d; v[ 37] = c.y-d; v[ 38] = c.z-d;
    v[ 39] = c.x-d; v[ 40] = c.y+d; v[ 41] = c.z+d;
    v[ 42] = c.x-d; v[ 43] = c.y+d; v[ 44] = c.z-d;
    v[ 45] = c.x+d; v[ 46] = c.y-d; v[ 47] = c.z+d;
    v[ 48] = c.x-d; v[ 49] = c.y-d; v[ 50] = c.z+d;
    v[ 51] = c.x-d; v[ 52] = c.y-d; v[ 53] = c.z-d;
    v[ 54] = c.x-d; v[ 55] = c.y+d; v[ 56] = c.z+d;
    v[ 57] = c.x-d; v[ 58] = c.y-d; v[ 59] = c.z+d;
    v[ 60] = c.x+d; v[ 61] = c.y-d; v[ 62] = c.z+d;
    v[ 63] = c.x+d; v[ 64] = c.y+d; v[ 65] = c.z+d;
    v[ 66] = c.x+d; v[ 67] = c.y-d; v[ 68] = c.z-d;
    v[ 69] = c.x+d; v[ 70] = c.y+d; v[ 71] = c.z-d;
    v[ 72] = c.x+d; v[ 73] = c.y-d; v[ 74] = c.z-d;
    v[ 75] = c.x+d; v[ 76] = c.y+d; v[ 77] = c.z+d;
    v[ 78] = c.x+d; v[ 79] = c.y-d; v[ 80] = c.z+d;
    v[ 81] = c.x+d; v[ 82] = c.y+d; v[ 83] = c.z+d;
    v[ 84] = c.x+d; v[ 85] = c.y+d; v[ 86] = c.z-d;
    v[ 87] = c.x-d; v[ 88] = c.y+d; v[ 89] = c.z-d;
    v[ 90] = c.x+d; v[ 91] = c.y+d; v[ 92] = c.z+d;
    v[ 93] = c.x-d; v[ 94] = c.y+d; v[ 95] = c.z-d;
    v[ 96] = c.x-d; v[ 97] = c.y+d; v[ 98] = c.z+d;
    v[ 99] = c.x+d; v[100] = c.y+d; v[101] = c.z+d;
    v[102] = c.x-d; v[103] = c.y+d; v[104] = c.z+d;
    v[105] = c.x+d; v[106] = c.y-d; v[107] = c.z+d;
}

//  See: https://www.khronos.org/opengl/wiki/Tutorial2:_VAOs,_VBOs,_Vertex_and_Fragment_Shaders_(C_/_SDL)
char * filetobuf(const char *file)
{
    FILE *fptr;
    long length;
    char *buf;

    fptr = fopen(file, "rb");
    if (!fptr)
        return NULL;
    fseek(fptr, 0, SEEK_END);
    length = ftell(fptr);
    buf = (char*)malloc(length+1);
    fseek(fptr, 0, SEEK_SET);
    fread(buf, length, 1, fptr);
    fclose(fptr);
    buf[length] = 0;

    return buf;
}

/**
 * Fake Game Client
*/
class FGClient
{
public:
    FGClient(boost::asio::io_context &io_context, const tcp::endpoint &server) : io_context_(io_context),
        socket_(io_context),
        signals_(io_context)
    {
        signals_.add(SIGINT);
        signals_.add(SIGTERM);
        signals_.async_wait(boost::bind(&FGClient::handle_stop, this));
        socket_.async_connect(server,
            boost::bind(&FGClient::handleConnect, this,
                boost::asio::placeholders::error));
    }

    void stop()
    {
        handle_stop();
    }

private:
    FGClient();

    void handleConnect(const boost::system::error_code &err)
    {
        if(!err)
        {
            boost::asio::async_read(socket_,
                boost::asio::buffer(read_buf_, FG_HEADER_LEN),
                boost::bind(&FGClient::handleReadHeader, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
        }
        else
        {
            std::cerr << err << std::endl;
        }
    }

    void handleReadHeader(const boost::system::error_code &err, size_t bytes_transferred)
    {
        if(!err)
        {
            //std::cout << "received " << bytes_transferred << std::endl;

            //  Try to read the header
            FGHeader h;
            h.read(read_buf_, bytes_transferred);

            if(h.magic == FG_MAGIC)
            {
                std::cout << h << std::endl;

                expected_message_type = (MESSAGE_TYPE)h.type;

                //  Read the payload
                boost::asio::async_read(socket_,
                    boost::asio::buffer(read_buf_, h.len),
                    boost::bind(&FGClient::handleReadPayload, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
            }
            else
            {
                std::cout << "wrong magic" << std::endl;

                //  Read another header
                boost::asio::async_read(socket_,
                    boost::asio::buffer(read_buf_, FG_HEADER_LEN),
                    boost::bind(&FGClient::handleReadHeader, this,
                        boost::asio::placeholders::error,
                        boost::asio::placeholders::bytes_transferred));
            }
        }
        else
        {
            std::cerr << err << std::endl;
        }
    }

    void handleReadPayload(const boost::system::error_code &err, size_t bytes_transferred)
    {
        if(!err)
        {
            //std::cout << "received " << bytes_transferred << std::endl;

            switch(expected_message_type)
            {
            case MESSAGE_TYPE::ENTITY_STATE_UPDATE:
                {
                    EntityState s;
                    s.read(read_buf_, bytes_transferred);
                    std::cout << s << std::endl;

                    if(entity_map.find(s.id) != entity_map.end())
                    {
                        //  This entity is already in the map, update it
                        *entity_map[s.id] = s;
                    }
                    else
                    {
                        //  This entity is not in the map, add it
                        std::shared_ptr<EntityState> p = std::make_shared<EntityState>(s);
                        entity_map[s.id] = p;
                    }
                }
                break;
            default:
                break;
            }

            //  Read another header
            boost::asio::async_read(socket_,
                boost::asio::buffer(read_buf_, FG_HEADER_LEN),
                boost::bind(&FGClient::handleReadHeader, this,
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
        }
        else
        {
            std::cerr << err << std::endl;
        }
    }

    void handle_stop()
    {
        std::cout << "boost signal handler" << std::endl;
        quit = true;
        io_context_.stop();
    }

    boost::asio::io_context& io_context_;
    tcp::socket socket_;
    boost::asio::signal_set signals_;
    uint8_t read_buf_[1024];
    MESSAGE_TYPE expected_message_type;
};

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}

glm::vec3 cameraPos   = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp    = glm::vec3(0.0f, 1.0f, 0.0f);

bool firstMouse = true;
float yaw   = -90.0f;
float pitch =  0.0f;
float lastX =  800.0f / 2.0;
float lastY =  600.0 / 2.0;

float deltaTime = 0.0f;
float lastFrame = 0.0f;

void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, true);
    }

    float cameraSpeed = static_cast<float>(2.5 * deltaTime);
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        cameraPos += cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        cameraPos -= cameraSpeed * cameraFront;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}

void sighandler(int signum)
{
    if(signum == SIGINT)
    {
        std::cout << "posix signal handler" << std::endl;
        quit = true;
    }
}

boost::asio::io_context io_context;
void tcpClientThreadFn()
{
    FGClient client(io_context, tcp::endpoint(tcp::v4(), 6208));

    io_context.run();
}

void mouse_callback(GLFWwindow* window, double xposIn, double yposIn)
{
    float xpos = static_cast<float>(xposIn);
    float ypos = static_cast<float>(yposIn);

    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
    lastX = xpos;
    lastY = ypos;

    float sensitivity = 0.1f; // change this value to your liking
    xoffset *= sensitivity;
    yoffset *= sensitivity;

    yaw += xoffset;
    pitch += yoffset;

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    if (pitch > 89.0f)
        pitch = 89.0f;
    if (pitch < -89.0f)
        pitch = -89.0f;

    glm::vec3 front;
    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
    front.y = sin(glm::radians(pitch));
    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
    cameraFront = glm::normalize(front);
}

int main(int argc, char *argv[])
{
    signal(SIGINT, sighandler);

    //  Make some fake entities to test rendering
    const size_t MAX_ENTITIES = 1000;

    std::thread tcpClientThread(tcpClientThreadFn);

    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
    
    GLFWwindow* window = glfwCreateWindow(800, 600, "ayy lmao", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return EXIT_FAILURE;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);

    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    GLenum err = glewInit();
    if(err != GLEW_OK) {
        // Problem: glewInit failed, something is seriously wrong.
        std::cout << "glewInit failed: " << glewGetErrorString(err) << std::endl;
        return EXIT_FAILURE;
    }
    
    glEnable(GL_DEPTH_TEST);
    
    // vertex shader
    GLchar *vertex_shader_source = filetobuf("../src/shader.vert");
    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_shader_source, NULL);
    glCompileShader(vertex_shader);

    int success;
    char infoLog[512];
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        std::cout << "bad1" << std::endl;
        glGetShaderInfoLog(vertex_shader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    // fragment shader
    GLchar *fragment_shader_source = filetobuf("../src/shader.frag");
    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_shader_source, NULL);
    glCompileShader(fragment_shader);
    
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        std::cout << "bad2" << std::endl;
        glGetShaderInfoLog(fragment_shader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;
    }

    // link shaders
    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertex_shader);
    glAttachShader(shaderProgram, fragment_shader);
    glLinkProgram(shaderProgram);

    // check for linking errors
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success)
    {
        std::cout << "bad3" << std::endl;
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    const size_t NUM_VERTICES = 36 * MAX_ENTITIES;
    float vertices[NUM_VERTICES * 3];

    GLuint vao;
    glGenVertexArrays(1, &vao);

    GLuint vbo;
    glGenBuffers(1, &vbo);
    
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), NULL, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), 0);
    glEnableVertexAttribArray(0);

    while(!glfwWindowShouldClose(window))
    {
        float currentFrame = static_cast<float>(glfwGetTime());
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        processInput(window);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // draw our first triangle
        glUseProgram(shaderProgram);

        //  Get entity positions
        //for(size_t i=0; i<NUM_ENTITIES; i++)
        size_t i = 0;
        for(auto e : entity_map)
        {
            /*const glm::vec3 epos = glm::vec3(
                (float)entity_map[i]->pos.x,
                (float)entity_map[i]->pos.y,
                (float)entity_map[i]->pos.z);*/
            const glm::vec3 epos = glm::vec3(
                (float)e.second->pos.x,
                (float)e.second->pos.y,
                (float)e.second->pos.z);
            //make_cube_vertices(glm::vec3(0.0f, 0.0f, 0.0f), vertices + i * 36 * 3);
            //make_cube_vertices(glm::vec3((float)(i / 10.0), 0.0f, 0.0f), vertices + i * 36 * 3);
            makeCubeVertices(epos, vertices + i * 36 * 3, 108);
            i++;
        }
        glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), &vertices);
        
        /*void *p = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
        memcpy(p, vertices, sizeof(vertices));
        glUnmapBuffer(GL_ARRAY_BUFFER);
        p = nullptr;*/

        glm::mat4 model         = glm::mat4(1.0f);
        glm::mat4 view          = glm::mat4(1.0f);
        glm::mat4 projection    = glm::mat4(1.0f);
        //view       = glm::translate(view, glm::vec3(0.0f, 0.0f, -3.0f));
        view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
        projection = glm::perspective(glm::radians(45.0f), 800.0f / 600.0f, 0.1f, 100.0f);

        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "model"), 1, GL_FALSE, &model[0][0]);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "view"), 1, GL_FALSE, &view[0][0]);
        glUniformMatrix4fv(glGetUniformLocation(shaderProgram, "projection"), 1, GL_FALSE, &projection[0][0]);

        glBindVertexArray(vao); // seeing as we only have a single VAO there's no need to bind it every time, but we'll do so to keep things a bit more organized
        //glDrawArrays(GL_TRIANGLES, 0, 6);
        //glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        //glDrawArrays(GL_POINTS, 0, 8);
        //glDrawArrays(GL_LINE_LOOP, 0, 8);
        //glDrawArrays(GL_TRIANGLES, 0, 12*3);

        for(size_t i=0; i<entity_map.size(); i++)
        {
            glDrawArrays(GL_TRIANGLES, i * 36, 36);
        }

        glfwSwapBuffers(window);
        glfwPollEvents();    
    }
    quit = true;
    io_context.stop();
    
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    glDeleteProgram(shaderProgram);
    
    glfwTerminate();

    tcpClientThread.join();

    std::cout << "done" << std::endl;

    return EXIT_SUCCESS;
}
#include "fake_game/serialization.hpp"

#include <bit>
#include <cstring>

namespace fake_game
{

union uint_double
{
    uint64_t ui_val;
    double d_val;
};

size_t read(const uint8_t * const buf, const size_t &buf_len, uint8_t &value)
{
    if(buf_len < 1)
    {
        return 0;
    }

    value = *buf;

    return 1;
}

size_t read(const uint8_t * const buf, const size_t &buf_len, uint16_t &value)
{
    if(buf_len < 2)
    {
        return 0;
    }

    value = *buf;
    value <<= 8;
    value |= *(buf + 1);

    return 2;
}

size_t read(const uint8_t * const buf, const size_t &buf_len, uint32_t &value)
{
    if(buf_len < 4)
    {
        return 0;
    }

    value = *buf;
    value <<= 8;
    value |= *(buf + 1);
    value <<= 8;
    value |= *(buf + 2);
    value <<= 8;
    value |= *(buf + 3);

    return 4;
}

size_t read(const uint8_t * const buf, const size_t &buf_len, double &value)
{
    if(buf_len < 8)
    {
        return 0;
    }

    uint_double tmp;
    tmp.ui_val = *buf;
    tmp.ui_val <<= 8;
    tmp.ui_val |= *(buf + 1);
    tmp.ui_val <<= 8;
    tmp.ui_val |= *(buf + 2);
    tmp.ui_val <<= 8;
    tmp.ui_val |= *(buf + 3);
    tmp.ui_val <<= 8;
    tmp.ui_val |= *(buf + 4);
    tmp.ui_val <<= 8;
    tmp.ui_val |= *(buf + 5);
    tmp.ui_val <<= 8;
    tmp.ui_val |= *(buf + 6);
    tmp.ui_val <<= 8;
    tmp.ui_val |= *(buf + 7);

    value = tmp.d_val;
    
    return 8;
}

size_t write(uint8_t * const buf, const size_t &buf_len, const uint8_t &value)
{
    if(buf_len < 1)
    {
        return 0;
    }

    *buf = value;

    return 1;
}

size_t write(uint8_t * const buf, const size_t &buf_len, const uint16_t &value)
{
    if(buf_len < 2)
    {
        return 0;
    }

    * buf      = (value >> 8) & 0xff;
    *(buf + 1) =  value       & 0xff;

    return 2;
}

size_t write(uint8_t * const buf, const size_t &buf_len, const uint32_t &value)
{
    if(buf_len < 4)
    {
        return 0;
    }

    * buf      = (value >> 24) & 0xff;
    *(buf + 1) = (value >> 16) & 0xff;
    *(buf + 2) = (value >> 8)  & 0xff;
    *(buf + 3) =  value        & 0xff;

    return 4;
}

size_t write(uint8_t * const buf, const size_t &buf_len, const double &value)
{
    if(buf_len < 8)
    {
        return 0;
    }

    uint_double tmp;
    tmp.d_val = value;

    * buf      = (tmp.ui_val >> 56) & 0xff;
    *(buf + 1) = (tmp.ui_val >> 48) & 0xff;
    *(buf + 2) = (tmp.ui_val >> 40) & 0xff;
    *(buf + 3) = (tmp.ui_val >> 32) & 0xff;
    *(buf + 4) = (tmp.ui_val >> 24) & 0xff;
    *(buf + 5) = (tmp.ui_val >> 16) & 0xff;
    *(buf + 6) = (tmp.ui_val >> 8)  & 0xff;
    *(buf + 7) =  tmp.ui_val        & 0xff;

    return 8;
}

}
#version 330 core

layout (location = 0) in vec3 in_Position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main(void) {
    vec3 scaled_pos = in_Position / 1000.0f;
    gl_Position = projection * view * model * vec4(scaled_pos, 1.0);
    //gl_Position = vec4(in_Position, 1.0);
}